@extends('layouts.plantilla')
@section('contenido')

  

<div class="pagetitle">
      <h1>Nueva marca</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Nueva marca</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Registre nueva marca
</div>
  
<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-6 position-relative">
    <label for="validationTooltip03" class="form-label">Marca</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Ingrese una marca" required>
    <div class="invalid-tooltip">
      Por favor ingrese una marca
    </div>
  </div>



  <div class="col-md-6 position-relative">
  </div>


  <div class="col-12">
    <button class="btn btn-primary" type="submit">Registrar</button>
  </div>
</form>

</div>
</section>
@endsection