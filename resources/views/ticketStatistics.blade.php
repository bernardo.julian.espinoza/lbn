@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Estadisticas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Estadisticas</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
      <div class="row">

            <!-- Budget Report -->
            <div class="col-lg-12">
          <div class="row">

            <!-- Budget Report -->
          <div>
           
            <div class="card-body pb-0">
              <h5 class="card-title">Tipos de eventos <span>| Este mes</span></h5>

              <div id="budgetChart" style="min-height: 400px;" class="echart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  var budgetChart = echarts.init(document.querySelector("#budgetChart")).setOption({
                    legend: {
                      data: ['Red', 'Video', 'Camaras', 'Tren control', 'Energia']
                    },
                    radar: {
                      // shape: 'circle',
                      indicator: [{
                          name: 'Falla',
                          max: 6500
                        },
                        {
                          name: 'Rotura',
                          max: 16000
                        },
                        {
                          name: 'Defecto',
                          max: 30000
                        },
                        {
                          name: 'Modificacion',
                          max: 38000
                        },
                        {
                          name: 'Acceso',
                          max: 52000
                        },
                        {
                          name: 'Desconexion',
                          max: 25000
                        }
                      ]
                    },
                    series: [{
                      name: 'Budget vs spending',
                      type: 'radar',
                      data: [{
                          value: [4200, 3000, 20000, 35000, 50000, 18000],
                          name: 'Red'
                        },
                        {
                          value: [5000, 14000, 28000, 26000, 42000, 21000],
                          name: 'Video'
                        },
                        {
                          value: [5000, 14000, 2000, 26000, 4000, 21000],
                          name: 'Camaras'
                        },
                        {
                          value: [5000, 14000, 2800, 2000, 4200, 21000],
                          name: 'Tren control'
                        },
                        {
                          value: [5000, 1400, 28000, 24000, 42000, 2000],
                          name: 'Energia'
                        }

                      ]
                    }]
                  });
                });
              </script>

            </div>
          </div><!-- End Budget Report -->
        
 </div> 
</div>

 <div class="col-lg-12">
          
 <!-- Website Traffic -->
          <div>
          

            <div class="card-body pb-0">
              <h5 class="card-title">Tipo de categoria <span>| Este mes</span></h5>

              <div id="trafficChart" style="min-height: 400px;" class="echart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  echarts.init(document.querySelector("#trafficChart")).setOption({
                    tooltip: {
                      trigger: 'item'
                    },
                    legend: {
                      top: '5%',
                      left: 'center'
                    },
                    series: [{
                      name: 'Access From',
                      type: 'pie',
                      radius: ['40%', '70%'],
                      avoidLabelOverlap: false,
                      label: {
                        show: false,
                        position: 'center'
                      },
                      emphasis: {
                        label: {
                          show: true,
                          fontSize: '18',
                          fontWeight: 'bold'
                        }
                      },
                      labelLine: {
                        show: false
                      },
                      data: [{
                          value: 1048,
                          name: 'Red'
                        },
                        {
                          value: 735,
                          name: 'Video'
                        },
                        {
                          value: 580,
                          name: 'Camaras'
                        },
                        {
                          value: 484,
                          name: 'Tren control'
                        },
                        {
                          value: 300,
                          name: 'Energia'
                        }
                      ]
                    }]
                  });
                });
              </script>

            </div>
          </div><!-- End Website Traffic -->
         
          
        
 
</div>

          

</div>
</section>

          


@endsection