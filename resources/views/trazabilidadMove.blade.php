@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Nueva trazabilidad</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trazabilidad</a></li>
          <li class="breadcrumb-item active">Nueva trazabilidad</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Informacion del movimiento
</div>
  
<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-2 position-relative">
    <label for="validationTooltip03" class="form-label">CODIGO</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique codigo" required>
    <div class="invalid-tooltip">
      Numero de inventario infraestructura
    </div>
  </div>

<div class="col-md-5 position-relative">
</div>
<div class="col-md-5 position-relative">
</div>

  

  <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Desde</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Ubicacion de origen
    </div>
  </div>

 <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Hasta</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Ubicacion de destino
    </div>
  </div>

 <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Motivo</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Motivo del movimiento
    </div>
  </div>
 



   <div class="col-md-12 position-relative">
    <label for="validationTextarea" class="form-label">Comentarios</label>
    <textarea class="form-control" id="validationTextarea" placeholder="Comentarios del movimiento" required></textarea>
    <div class="invalid-feedback">
      Por favor ingrese el comentario acerca del movimiento
    </div>
  </div>


  <div class="col-12">
    <button class="btn btn-primary" type="submit">Registrar</button>
  </div>
</form>



</div>
</section>


@endsection