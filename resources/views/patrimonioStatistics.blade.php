@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Estadisticas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Estadisticas</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
      <div class="row">

           

 <div class="col-lg-12">
          
 <!-- Website Traffic -->
          <div>
          

            <div class="card-body pb-0">
              <h5 class="card-title">Patrimonio<span>| Total</span></h5>

              <div id="trafficChart" style="min-height: 400px;" class="echart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  echarts.init(document.querySelector("#trafficChart")).setOption({
                    tooltip: {
                      trigger: 'item'
                    },
                    legend: {
                      top: '5%',
                      left: 'center'
                    },
                    series: [{
                      name: 'Access From',
                      type: 'pie',
                      radius: ['40%', '70%'],
                      avoidLabelOverlap: false,
                      label: {
                        show: false,
                        position: 'center'
                      },
                      emphasis: {
                        label: {
                          show: true,
                          fontSize: '18',
                          fontWeight: 'bold'
                        }
                      },
                      labelLine: {
                        show: false
                      },
                      data: [{
                          value: 1048,
                          name: 'DVR'
                        },
                        {
                          value: 735,
                          name: 'UPS'
                        },
                        {
                          value: 580,
                          name: 'CAMARAS'
                        },
                        {
                          value: 484,
                          name: 'TELEFONO IP'
                        },
                        {
                          value: 300,
                          name: 'TELEFONO'
                        },
                        {
                          value: 300,
                          name: 'CAMARA HDCVI'
                        },
                        {
                          value: 300,
                          name: 'CAMARA'
                        },
                        {
                          value: 300,
                          name: 'FUENTE'
                        }
                      ]
                    }]
                  });
                });
              </script>

            </div>
          </div><!-- End Website Traffic -->
         
          
        
 
</div>

          

</div>
</section>

          


@endsection