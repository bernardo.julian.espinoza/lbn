@include('layouts.head')
@include('layouts.header')
@include('layouts.sidebar')

<main id="main" class="main">
@yield('contenido')
</main><!-- End #main -->

@include('layouts.footer')
