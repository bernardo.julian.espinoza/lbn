<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

<ul class="sidebar-nav" id="sidebar-nav">
<li class="nav-item">
    <a class="nav-link " href="/dashboard">
      <i class="bi bi-grid"></i>
      <span>Panel principal</span>
    </a>
  </li><!-- End Dashboard Nav -->


   
<li class="nav-heading">Administracion</li>

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-person"></i><span>Cuentas</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="/user/create">
          <i class="bi bi-circle"></i><span>Crear usuario</span>
        </a>
      </li>
      <li>
        <a href="/user/creategroup">
          <i class="bi bi-circle"></i><span>Crear grupo</span>
        </a>
      </li>
      <li>
        <a href="/user/manageuser">
          <i class="bi bi-circle"></i><span>Administrar usuarios</span>
        </a>
      </li>
      <li>
        <a href="/user/managegroup">
          <i class="bi bi-circle"></i><span>Administrar grupos</span>
        </a>
      </li>
     
    </ul>
  </li><!-- End Components Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-card-list"></i><span>Tickets</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="/ticket/create">
          <i class="bi bi-circle"></i><span>Nuevo ticket</span>
        </a>
      </li>
      <li>
        <a href="/ticket/manage">
          <i class="bi bi-circle"></i><span>Gestion de tickets</span>
        </a>
      </li>
      <li>
        <a href="/ticket/search">
          <i class="bi bi-circle"></i><span>Buscar ticket</span>
        </a>
      </li>
      <li>
        <a href="/ticket/state">
          <i class="bi bi-circle"></i><span>Estado de solicitudes</span>
        </a>
      </li>
      <li>
        <a href="/ticket/report">
          <i class="bi bi-circle"></i><span>Reportes</span>
        </a>
      </li>
    
      <li>
        <a href="/ticket/statistics">
          <i class="bi bi-circle"></i><span>Estadisticas</span>
        </a>
      </li>

      <li>
        <a href="/ticket/inbox">
          <i class="bi bi-circle"></i><span>Bandeja de novedades</span>
        </a>
      </li>
    </ul>
  </li><!-- End Forms Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-gem"></i><span>Patrimonio</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="/patrimonio/create">
          <i class="bi bi-circle"></i><span>Nuevo patrimonio</span>
        </a>
      </li>
      <li>
        <a href="/patrimonio/search">
          <i class="bi bi-circle"></i><span>Buscar patrimonio</span>
        </a>
      </li>
      <li>
        <a href="/patrimonio/brand">
          <i class="bi bi-circle"></i><span>Nueva marca</span>
        </a>
      </li>
      <li>
        <a href="/patrimonio/model">
          <i class="bi bi-circle"></i><span>Nuevo modelo</span>
        </a>
      </li>
      <li>
        <a href="/patrimonio/report">
          <i class="bi bi-circle"></i><span>Reportes</span>
        </a>
      </li>
      <li>
        <a href="/patrimonio/statistics">
          <i class="bi bi-circle"></i><span>Estadisticas</span>
        </a>
      </li>
    </ul>
  </li><!-- End Tables Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#charts-nav" data-bs-toggle="collapse" href="#">
      <i class="bi bi-bar-chart"></i><span>Trazabilidad</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="charts-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
 
 
    <li>
        <a href="/trazabilidad/operation">
          <i class="bi bi-circle"></i><span>Nueva operacion</span>
        </a>
      </li>  

    <li>
        <a href="/trazabilidad/move">
          <i class="bi bi-circle"></i><span>Nuevo movimiento</span>
        </a>
      </li>  
    <li>
        <a href="/trazabilidad/manage">
          <i class="bi bi-circle"></i><span>Gestion de movimientos</span>
        </a>
      </li>
      <li>
        <a href="/trazabilidad/search">
          <i class="bi bi-circle"></i><span>Busqueda de movimientos</span>
        </a>
      </li>
      <li>
        <a href="/trazabilidad/report">
          <i class="bi bi-circle"></i><span>Reportes</span>
        </a>
      </li>
      <li>
        <a href="/trazabilidad/statistics">
          <i class="bi bi-circle"></i><span>Estadisticas</span>
        </a>
      </li>
    </ul>
  </li><!-- End Charts Nav -->

  <li class="nav-heading">Contratistas</li>


  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#iconsx-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-layout-text-window-reverse"></i><span>MKS</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="iconsx-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="/contratista/dailyform">
          <i class="bi bi-circle"></i><span>Formulario diario</span>
        </a>
      </li>
      <li>
        <a href="/contratista/semesterform">
          <i class="bi bi-circle"></i><span>Formulario semestral</span>
        </a>
      </li>
      <li>
        <a href="/contratista/annualreport">
          <i class="bi bi-circle"></i><span>Informe anual</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->



  
<li class="nav-heading">Servicios en linea</li>


<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons1-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-ethernet"></i><span>VLAN1-Red</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons1-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v1.infra.snmp" target="_blank">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v1.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v1.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v1.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->


  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons10-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-camera-reels"></i><span>VLAN10-Video</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons10-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v10.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v10.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v10.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v10.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons20-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-camera-video-fill"></i><span>VLAN20-Camaras</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons20-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v20.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v20.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v20.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v20.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons30-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-google"></i><span>VLAN30-Web</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons30-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v30.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v30.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v30.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v30.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->


<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons40-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-server"></i><span>VLAN40-Almacenamiento</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons40-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v40.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v40.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v40.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v40.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons50-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-battery-charging"></i><span>VLAN50-Energia</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons50-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v50.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v50.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v50.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v50.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons60-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-fingerprint"></i><span>VLAN60-Control de acceso</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons60-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v60.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v60.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v60.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v60.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons70-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-telephone-forward"></i><span>VLAN70-Tren control</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons70-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v70.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v70.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v70.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v70.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

<li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons80-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-pc-display-horizontal"></i><span>VLAN80-Telecomando</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons80-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v80.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v80.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v80.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v80.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

  <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#icons90-nav" data-bs-toggle="collapse" href="#">
    <i class="bi bi-telephone-fill"></i><span>VLAN90-VOIP</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="icons90-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
      <li>
        <a href="http://v90.infra.snmp">
          <i class="bi bi-circle"></i><span>Monitorear estado</span>
        </a>
      </li>
      <li>
        <a href="http://v90.infra.db">
          <i class="bi bi-circle"></i><span>Ver base de datos</span>
        </a>
      </li>
      <li>
        <a href="http://v90.infra.ntp">
          <i class="bi bi-circle"></i><span>Ver estado NTP</span>
        </a>
      </li>
      <li>
        <a href="http://v90.infra.proxy">
          <i class="bi bi-circle"></i><span>Ver estado Proxy</span>
        </a>
      </li>
    </ul>
  </li><!-- End Icons Nav -->

<li class="nav-heading">Otros</li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="www.docs.google.com">
    <i class="bi bi-journal-text"></i>
      <span>Notas</span>
    </a>
  </li><!-- End Profile Page Nav -->

 
  <li class="nav-item">
    <a class="nav-link collapsed" href="www.mail.google.com">
      <i class="bi bi-envelope"></i>
      <span>Correo electronico</span>
    </a>
  </li><!-- End Contact Page Nav -->

 <li class="nav-item">
    <a class="nav-link collapsed" href="#">
      <i class="bi bi-question-circle"></i>
      <span>Acerca de</span>
    </a>
  </li><!-- End F.A.Q Page Nav -->

</ul>

</aside><!-- End Sidebar-->