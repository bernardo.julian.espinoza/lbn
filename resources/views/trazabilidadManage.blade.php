@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Gestion de movimientos</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trazabilidad</a></li>
          <li class="breadcrumb-item active">Gestion de movimientos</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Aprobacion de movimientos
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Movimientos <span>| Nuevos</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Desde</th>
                        <th scope="col">Hasta</th>
                        <th scope="col">Motivo</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Comentarios</th>
                                             
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>
                     <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>
                      <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>
                      <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>
                      <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>
                      <tr>
                        <th scope="row"><a href="/trazabilidad/read" class="text-primary">#2457</a></th>
                        <td><a>Retiro</a></td>
                        <td><a>Boulogne</a></td>
                        <td>Instalacion</td>
                        <td>Javier Baldeon</td>
                        <td>Instalacion de camaras</td>
                             
                      </tr>

                    </tbody>
                  </table>

                </div>

             
            </div><!-- End Recent Sales -->
         
          </div>
        </section>
@endsection