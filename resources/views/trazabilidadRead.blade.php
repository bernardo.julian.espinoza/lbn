@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Datos del codigo n° 2344</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trazabilidad</a></li>
          <li class="breadcrumb-item"><a href="#">Gestion de trazabilidad</a></li>
          <li class="breadcrumb-item active">Datos del codigo</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Detalles del movimiento
</div>

<dl class="row">
   <dt class="col-sm-3">Propietario:</dt>
  <dd class="col-sm-3">Javier Oses</dd>

  <dt class="col-sm-3">Origen:</dt>
  <dd class="col-sm-3">Boulogne</dd>

  <dt class="col-sm-3">Motivo:</dt>
  <dd class="col-sm-3">Mantenimiento</dd>

  <dt class="col-sm-3">Destino:</dt>
  <dd class="col-sm-3">Retiro</dd>

  <dt class="col-sm-3">Detalle:</dt>
  <dd class="col-sm-9">Detalle del movimiento por parte del propietario</dd>

 </dl>


</div>
<button type="button" class="btn btn-success btn-sm">Aprobar</button>
<button type="button" class="btn btn-danger btn-sm">Rechazar</button>
</section>
@endsection