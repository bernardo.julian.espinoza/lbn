@extends('layouts.plantilla')
@section('contenido')
 

<div class="pagetitle">
      <h1>Reportes</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Reportes</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Filtrar por
</div>
  
<form class="row g-3">	


  <div class="col-md-3 position-relative">
    <label class="form-label">Marca</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>

  <div class="col-md-3 position-relative">
    <label class="form-label">Modelo</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
    <label class="form-label">Tipo</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
    <label class="form-label">Sector</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>

<div class="col-md-3 position-relative">
    <label class="form-label">Lugar inicial</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
    <label class="form-label">Creado por</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
    <label class="form-label">Estado</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>   
   </div>
   <div class="col-md-3 position-relative">
    <label class="form-label">Rubro</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>   
   </div>

  <div class="col-12">
    <button class="btn btn-primary" type="submit">Filtrar</button>
  </div>
</form>

<p></p>  

<div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Resultados
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Encontrados</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Serie</th>
                        <th scope="col">Patrimonio</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Alta</th>
                        <th scope="col">Creo</th>
                        <th scope="col">Estado</th>                                    
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a href="/patrimonio/read" class="text-primary">#2457</a></th>
                        <td>2dda997329</td>
                        <td>2082080280</td>
                        <td>Dahua</td>
                        <td>DHI-2118-EZ</td>
                        <td>NVR</td>
                        <td>Video</td>
                        <td>Central</td>
                        <td>22/11/2022</td>
                        <td>Javier Oses</td>
                        <td>activo</td>

                      </tr>
                      <tr>
                        <th scope="row"><a href="/patrimonio/read" class="text-primary">#2457</a></th>
                        <td>2dda997329</td>
                        <td>2082080280</td>
                        <td>Dahua</td>
                        <td>DHI-2118-EZ</td>
                        <td>NVR</td>
                        <td>Video</td>
                        <td>Central</td>
                        <td>22/11/2022</td>
                        <td>Javier Oses</td>
                        <td>activo</td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="/patrimonio/read" class="text-primary">#2457</a></th>
                        <td>2dda997329</td>
                        <td>2082080280</td>
                        <td>Dahua</td>
                        <td>DHI-2118-EZ</td>
                        <td>NVR</td>
                        <td>Video</td>
                        <td>Central</td>
                        <td>22/11/2022</td>
                        <td>Javier Oses</td>
                        <td>activo</td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="/patrimonio/read" class="text-primary">#2457</a></th>
                        <td>2dda997329</td>
                        <td>2082080280</td>
                        <td>Dahua</td>
                        <td>DHI-2118-EZ</td>
                        <td>NVR</td>
                        <td>Video</td>
                        <td>Central</td>
                        <td>22/11/2022</td>
                        <td>Javier Oses</td>
                        <td>activo</td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="/patrimonio/read" class="text-primary">#2457</a></th>
                        <td>2dda997329</td>
                        <td>2082080280</td>
                        <td>Dahua</td>
                        <td>DHI-2118-EZ</td>
                        <td>NVR</td>
                        <td>Video</td>
                        <td>Central</td>
                        <td>22/11/2022</td>
                        <td>Javier Oses</td>
                        <td>activo</td>
                      </tr>

                    </tbody>
                  </table>

                </div>
            </div><!-- End Recent Sales -->
<div class="d-grid gap-2 d-md-flex justify-content-md-end">
  <button class="btn btn-primary me-md-2" type="button">Exportar A PDF</button>
  <button class="btn btn-primary" type="button">Exportar a EXCEL</button>
</div>

</div>
</section>

@endsection