@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Buscar patrimonio</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Buscar patrimonio</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    
     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Identificacion de patrimonio
</div>

<form class="row g-3 needs-validation" novalidate>

  <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Identificacion</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Seleccione un tipo de identificacion
    </div>
  </div>

  <div class="col-md-8 position-relative">
  </div> 

	<div class="col-md-6 position-relative">
    <label for="validationTooltip03" class="form-label">Numero / codigo</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Ingrese codigo, serie o patrimonio" required>
    <div class="invalid-tooltip">
      Ingrese codigo, serie o patrimonio
    </div>
  </div>
	<div class="col-md-6 position-relative">
	</div>
  <div class="col-12">
    <button class="btn btn-primary" type="submit">Buscar</button>
  </div>
</form>
<p>
</p>	
 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Datos del patrimonio
</div>

<dl class="row">
  <dt class="col-sm-3">CODIGO:</dt>
  <dd class="col-sm-9">1212</dd>

  <dt class="col-sm-3">Serie:</dt>
  <dd class="col-sm-3">27937h993yh3hd</dd>


  <dt class="col-sm-3">Patrimonio:</dt>
  <dd class="col-sm-3">29792796297263</dd>

  <dt class="col-sm-3">Marca:</dt>
  <dd class="col-sm-3">Dahua</dd>

  <dt class="col-sm-3">Modelo:</dt>
  <dd class="col-sm-3">DHI-7218</dd>

  
  <dt class="col-sm-3">Tipo:</dt>
  <dd class="col-sm-3">Camara IP</dd>

  <dt class="col-sm-3">Creado por:</dt>
  <dd class="col-sm-3">Javier Baldeon</dd>

  <dt class="col-sm-3">Estado:</dt>
  <dd class="col-sm-9">Activo-Baja</dd>

  <dt class="col-sm-3">Creado el:</dt>
  <dd class="col-sm-9">12/12/22</dd>

  <dt class="col-sm-3">Caracteristicas:</dt>
  <dd class="col-sm-9">12vcc - 24vac</dd>



           
  
</div>
</section>
@endsection