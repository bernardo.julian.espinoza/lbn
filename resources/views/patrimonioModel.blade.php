@extends('layouts.plantilla')
@section('contenido')
  

<div class="pagetitle">
      <h1>Nuevo modelo</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Nuevo modelo</li>
        </ol>
      </nav>
    </div>
    <!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Seleccione marca e ingrese nuevo modelo
</div>
  
<form class="row g-3 needs-validation" novalidate>


  <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Marca</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Por favor seleccione la marca
    </div>
  </div>


	<div class="col-md-4 position-relative">
    <label for="validationTooltip03" class="form-label">Modelo</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Ingrese modelo" required>
    <div class="invalid-tooltip">
      Por favor ingrese el modelo
    </div>
  </div>



  <div class="col-md-4 position-relative">
  </div>


  <div class="col-12">
    <button class="btn btn-primary" type="submit">Enviar</button>
  </div>
</form>

</div>
</section>
@endsection