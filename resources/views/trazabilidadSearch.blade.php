@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Buscar movimientos</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trazabilidad</a></li>
          <li class="breadcrumb-item active">Buscar movimientos</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    
     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Identificacion del patrimonio
</div>

<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-3 position-relative">
    <label for="validationTooltip03" class="form-label">N°</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique numero de patrimonio" required>
    <div class="invalid-tooltip">
      N°
    </div>
  </div>

  <div class="col-12">
    <button class="btn btn-primary" type="submit">Buscar</button>
  </div>
</form>
<p>
</p>	
 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Datos del patrimonio N° 2333
</div>

<dl class="row">
  <dt class="col-sm-3">CODIGO:</dt>
  <dd class="col-sm-9">1212</dd>

  <dt class="col-sm-3">Serie:</dt>
  <dd class="col-sm-3">27937h993yh3hd</dd>


  <dt class="col-sm-3">Patrimonio:</dt>
  <dd class="col-sm-3">29792796297263</dd>

  <dt class="col-sm-3">Marca:</dt>
  <dd class="col-sm-3">Dahua</dd>

  <dt class="col-sm-3">Modelo:</dt>
  <dd class="col-sm-3">DHI-7218</dd>

  
  <dt class="col-sm-3">Tipo:</dt>
  <dd class="col-sm-3">Camara IP</dd>

  <dt class="col-sm-3">Creado por:</dt>
  <dd class="col-sm-3">Javier Baldeon</dd>

  <dt class="col-sm-3">Estado:</dt>
  <dd class="col-sm-9">Activo-Baja</dd>

  <dt class="col-sm-3">Creado el:</dt>
  <dd class="col-sm-9">12/12/22</dd>

  <dt class="col-sm-3">Caracteristicas:</dt>
  <dd class="col-sm-9">12vcc - 24vac</dd>

<p></p>

 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Movimientos del patrimonio N° 2333
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Trazabilidad<span>| Movimientos</span></h5>

                  <table class="table table-borderless">
                    <thead>
                      <tr>
                       
                        <th scope="col">Fecha</th>
                        <th scope="col">Desde</th>
                        <th scope="col">Hasta</th>
                        <th scope="col">Motivo</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Comentarios</th>

                      </tr>

                     </thead>
                    <tbody>
                      <tr>
                        
                        <td>2/3/2022</td>
                        <td>Retiro</td>
                        <td>Boulogne</td>
                        <td>Mantenimiento</td>
                        <td>Movimiento</td>    
                        <td>Bernardo Espinoza</td>
                        <td>Movimiento por mantenimiento de camara en Boulogne</td>
                               
                      </tr>
                      <tr>
                        
                        <td>2/3/2022</td>
                        <td>Retiro</td>
                        <td>Boulogne</td>
                        <td>Mantenimiento</td>
                        <td>Movimiento</td>    
                        <td>Bernardo Espinoza</td>
                        <td>Movimiento por mantenimiento de camara en Boulogne</td>
                               
                      </tr>
                      <tr>
                        
                        <td>2/3/2022</td>
                        <td>Retiro</td>
                        <td>Boulogne</td>
                        <td>Mantenimiento</td>
                        <td>Movimiento</td>    
                        <td>Bernardo Espinoza</td>
                        <td>Movimiento por mantenimiento de camara en Boulogne</td>
                               
                      </tr>
                      

                    </tbody>
                  </table>

                </div>

            
            </div><!-- End Recent Sales -->
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
  <button class="btn btn-primary me-md-2" type="button">Exportar A PDF</button>
  <button class="btn btn-primary" type="button">Exportar a EXCEL</button>
</div>
</div>
</section>
@endsection