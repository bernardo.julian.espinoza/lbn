@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Buscar ticket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Buscar ticket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    
     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Numero de ticket
</div>

<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-3 position-relative">
    <label for="validationTooltip03" class="form-label">N°</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique un resumen" required>
    <div class="invalid-tooltip">
      N°
    </div>
  </div>

  <div class="col-12">
    <button class="btn btn-primary" type="submit">Buscar</button>
  </div>
</form>
<p>
</p>	
 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Datos del ticket N° 2333
</div>

<dl class="row">
	 <dt class="col-sm-3">Asunto:</dt>
  <dd class="col-sm-9">No se puede visualizar las camaras</dd>

  <dt class="col-sm-3">Propietario:</dt>
  <dd class="col-sm-3">Javier Baldeon</dd>


  <dt class="col-sm-3">Solicitud creada el:</dt>
  <dd class="col-sm-3">2/6/2022</dd>

  <dt class="col-sm-3">Supervisor a cargo:</dt>
  <dd class="col-sm-3">Javier Oses</dd>

  <dt class="col-sm-3">Solicitud cerrada el:</dt>
  <dd class="col-sm-3">2/8/2022</dd>

  
  <dt class="col-sm-3">Tecnico asignado:</dt>
  <dd class="col-sm-3">Javier Oses</dd>

  <dt class="col-sm-3">Lugar de intervencion:</dt>
  <dd class="col-sm-3">Oficina de monitoreo</dd>

  <dt class="col-sm-3">Descripcion:</dt>
  <dd class="col-sm-9">Descripcion por parte del propietario</dd>

  <dt class="col-sm-3">Detalles:</dt>
  <dd class="col-sm-9">Detalles agregados por el supervisor</dd>

  <dt class="col-sm-3">Observaciones:</dt>
  <dd class="col-sm-9">Observaciones registradas por el tecnico</dd>


</dl>

 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Estados del ticket N° 2333
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Ticket<span>| Cambios</span></h5>

                  <table class="table table-borderless">
                    <thead>
                      <tr>
                       
                        <th scope="col">Estado</th>
                        <th scope="col">Actualizado</th>
                        <th scope="col">Usuario</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">Prioridad</th>

                      </tr>

                     </thead>
                    <tbody>
                      <tr>
                        
                        <td><span class="badge bg-success">Abierto</span></td> 
                        <td>2/3/2022</td>
                        <td>Bernardo Espinoza</td>
                        <td>Video</td>
                        <td>Baja</td>
                               
                      </tr>
                       <tr>
                        
                        <td><span class="badge bg-success">Abierto</span></td> 
                        <td>2/3/2022</td>
                        <td>Javier Baldeon</td>
                        <td>Video</td>
                        <td>Baja</td>
                               
                      </tr>
                      

                    </tbody>
                  </table>

                </div>

            
            </div><!-- End Recent Sales -->
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
  <button class="btn btn-primary me-md-2" type="button">Exportar A PDF</button>
  <button class="btn btn-primary" type="button">Exportar a EXCEL</button>
</div>
</div>
</section>
@endsection