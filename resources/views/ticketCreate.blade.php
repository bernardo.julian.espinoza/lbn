@extends('layouts.plantilla')
@section('contenido')


    @if( session('mensaje') )
        <div class="alert alert-success">
            {{ session('mensaje') }}
        </div>
    @endif

<div class="pagetitle">
      <h1>Nuevo ticket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Nuevo ticket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Informacion
</div>
  
<form class="row g-3 needs-validation" action="/ticket/store" method="post" enctype="multipart/form-data" novalidate>
@csrf

<input  for="ticPropietario" id="ticPropietario" name="ticPropietario" type="hidden" value="2">


	<div class="col-md-9 position-relative">
    <label for="ticResumen" class="form-label">Indique un resumen</label>
    <input type="text" name="ticResumen" class="form-control" id="ticResumen" placeholder="Indique un resumen" value="{{ old('ticResumen')}}" required>
    <div class="invalid-tooltip">
      Por favor complete el resumen
    </div>
  </div>



  <div class="col-md-3 position-relative">
    <label for="ticUbicacion" class="form-label">Su ubicacion</label>
    <select class="form-select" name="ticUbicacion" id="ticUbicacion" required>
      <option selected disabled value="">Seleccione un lugar</option>
      @foreach( $ubicacions as $ubicacion )
                    <option @selected( old('idUbicacion')==$ubicacion->idUbicacion ) value="{{ $ubicacion->idUbicacion }}">{{ $ubicacion->ubiNombre }}</option>
            @endforeach
    </select>
    <div class="invalid-tooltip">
      Por favor seleccione un lugar
    </div>
  </div>

  <div class="col-md-6 position-relative">
  </div>

   <div class="col-md-12 position-relative">
    <label for="ticDescripcion" class="form-label">Descripcion</label>
    <textarea class="form-control" id="ticDescripcion" name="ticDescripcion" placeholder="Indique una descripcion" required>{{old('ticDescripcion')}}</textarea>
    <div class="invalid-feedback">
      Por favor complete el campo descripcion
    </div>
  </div>


  <div class="col-12">
    <button class="btn btn-primary">Enviar</button>
  </div>
</form>

</div>
@include( 'layouts.msgValidacion' )
</section>

@endsection
