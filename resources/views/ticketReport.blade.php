@extends('layouts.plantilla')
@section('contenido')
 

<div class="pagetitle">
      <h1>Reportes</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Reportes</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Filtrar por
</div>
  
<form class="row g-3">	


  <div class="col-md-3 position-relative">
    <label class="form-label">Propietario</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>

  <div class="col-md-3 position-relative">
    <label class="form-label">Supervisor</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
    <label class="form-label">Tecnico</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-3 position-relative">
  </div>

<div class="col-md-4 position-relative">
    <label class="form-label">Estado</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-4 position-relative">
    <label class="form-label">Prioridad</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-4 position-relative">
    <label class="form-label">Categoria</label>
    <select class="form-select" id="validationTooltip04">
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>   
   </div>

  <div class="col-12">
    <button class="btn btn-primary" type="submit">Filtrar</button>
  </div>
</form>

<p></p>  

<div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Resultados
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Encontrados</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Ticket</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Asunto</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">Lugar</th>
                        <th scope="col">Estado</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a href="/ticket/read" class="text-primary">#2457</a></th>
                        <td><a>Javier Perez</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td><a>Video</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                      <tr>
                         <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Tren Control</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Video</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Señalamiento</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td>Video</td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>

                    </tbody>
                  </table>

                </div>
            </div><!-- End Recent Sales -->
<div class="d-grid gap-2 d-md-flex justify-content-md-end">
  <button class="btn btn-primary me-md-2" type="button">Exportar A PDF</button>
  <button class="btn btn-primary" type="button">Exportar a EXCEL</button>
</div>

</div>
</section>

@endsection