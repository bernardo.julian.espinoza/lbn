@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Datos del ticket n° {{$ticket->idTicket}}</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item"><a href="#">Gestion de tickets</a></li>
          <li class="breadcrumb-item active">Datos del ticket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Ticket abierto
</div>

<dl class="row">
   <dt class="col-sm-3">Asunto:</dt>
  <dd class="col-sm-9">{{$ticket->ticResumen}}</dd>

  <dt class="col-sm-3">Propietario:</dt>
  <dd class="col-sm-9">{{$ticket->getPropietario->proNombre}}</dd>

  <dt class="col-sm-3">Solicitud creada el:</dt>
  <dd class="col-sm-9">{{$ticket->created_at}}</dd>

  <dt class="col-sm-3">Lugar de intervencion:</dt>
  <dd class="col-sm-9">{{$ticket->getUbicacion->ubiNombre}}</dd>

  <dt class="col-sm-3">Descripcion:</dt>
  <dd class="col-sm-9">{{$ticket->ticDescripcion}}</dd>

 </dl>

 <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Asignar y dar prioridad
</div>

<form action="/ticket/update" method="post" enctype="multipart/form-data" class="row g-3 needs-validation" novalidate>
  @method('put')
  @csrf
<input type="hidden" name="idTicket" value="{{$ticket->idTicket}}">

  <div class="col-md-3 position-relative">
    <label for="ticCategoria" class="form-label">Asignado a</label>
    <select class="form-select" name="ticCategoria" id="ticCategoria" required>
      <option selected disabled value="">Seleccione una categoria</option>
      @foreach( $categorias as $categoria )
                    <option @selected( old('idCategoria')==$categoria->idCategoria ) value="{{ $categoria->idCategoria }}">{{ $categoria->catNombre }}</option>
            @endforeach
    </select>
    <div class="invalid-tooltip">
      Seleccione una categoria
    </div>
  </div>

  <div class="col-md-3 position-relative">
    <label for="ticPrioridad" class="form-label">Prioridad</label>
    <select class="form-select" name="ticPrioridad" id="ticPrioridad" required>
      <option selected disabled value="">Seleccione una prioridad</option>
       @foreach( $prioridads as $prioridad )
                    <option @selected( old('idPrioridad')==$prioridad->idPrioridad ) value="{{ $prioridad->idPrioridad }}">{{ $prioridad->priNombre }}</option>
            @endforeach
    </select>
    <div class="invalid-tooltip">
      Seleccione una prioridad
    </div>
  </div>

  <div class="col-md-6 position-relative">
  </div>

<input  for="ticSupervisor" id="ticSupervisor" name="ticSupervisor" type="hidden" value="3">

   <div class="col-md-12 position-relative">
    <label for="ticDetalle" class="form-label">Detalle</label>
    <textarea class="form-control" name="ticDetalle" id="ticDetalle" placeholder="Indique una descripcion" required>{{old('ticDetalle')}}</textarea>
    <div class="invalid-feedback">
      Por favor complete el campo detalle
    </div>
  </div>


  <div class="col-12">
    <button class="btn btn-primary">Listo</button>
  </div>
</form>

</div>
</section>
@endsection