@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Estadisticas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trazabilidad</a></li>
          <li class="breadcrumb-item active">Estadisticas</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
      <div class="row">

           

 <div class="col-lg-12">
          
 <!-- Website Traffic -->
          <div>
          

            <div class="card-body pb-0">
              <h5 class="card-title">Patrimonio<span>| Total</span></h5>

              <div id="trafficChart" style="min-height: 400px;" class="echart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  echarts.init(document.querySelector("#trafficChart")).setOption({
                    tooltip: {
                      trigger: 'item'
                    },
                    legend: {
                      top: '5%',
                      left: 'center'
                    },
                    series: [{
                      name: 'Access From',
                      type: 'pie',
                      radius: ['40%', '70%'],
                      avoidLabelOverlap: false,
                      label: {
                        show: false,
                        position: 'center'
                      },
                      emphasis: {
                        label: {
                          show: true,
                          fontSize: '18',
                          fontWeight: 'bold'
                        }
                      },
                      labelLine: {
                        show: false
                      },
                      data: [{
                          value: 1048,
                          name: 'INSTALACION'
                        },
                        {
                          value: 735,
                          name: 'DESINSTALACION'
                        },
                        {
                          value: 580,
                          name: 'ALTA'
                        },
                        {
                          value: 484,
                          name: 'BAJA'
                        },
                        {
                          value: 300,
                          name: 'MODIFICACION'
                        },
                        {
                          value: 300,
                          name: 'VERIFICACION'
                        },
                        {
                          value: 300,
                          name: 'TRANSFERENCIA'
                        },
                        {
                          value: 300,
                          name: 'LIMPIEZA'
                        }
                      ]
                    }]
                  });
                });
              </script>

            </div>
          </div><!-- End Website Traffic -->
         
          
        
 
</div>

          

</div>
</section>

          


@endsection