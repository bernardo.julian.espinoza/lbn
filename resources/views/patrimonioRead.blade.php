@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Datos del patrimonio n° 2344</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item"><a href="/patrimonio/report">Reportes</a></li>
          <li class="breadcrumb-item active">Datos del patrimonio</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Informacion del patrimonio
</div>

<dl class="row">
  <dt class="col-sm-3">Codigo:</dt>
  <dd class="col-sm-3">6262</dd>

  <dt class="col-sm-3">Marca:</dt>
  <dd class="col-sm-3">Dahua</dd>

  <dt class="col-sm-3">Serie:</dt>
  <dd class="col-sm-3">729729727101</dd>

  <dt class="col-sm-3">Modelo:</dt>
  <dd class="col-sm-3">DHI-2101-EZ</dd>

  <dt class="col-sm-3">Patrimonio:</dt>
  <dd class="col-sm-3">80280270727</dd>

  <dt class="col-sm-3">Tipo:</dt>
  <dd class="col-sm-3">NVR</dd>
 
  <dt class="col-sm-3">Creado por:</dt>
  <dd class="col-sm-3">Javier Baldeon</dd>

  <dt class="col-sm-3">Rubro:</dt>
  <dd class="col-sm-3">Video</dd>

<dt class="col-sm-3">Fecha alta:</dt>
  <dd class="col-sm-3">22/12/2022</dd>

  <dt class="col-sm-3">Lugar inicio:</dt>
  <dd class="col-sm-3">Central</dd>
  

  <dt class="col-sm-3">Estado:</dt>
  <dd class="col-sm-9">Activo</dd>

  <dt class="col-sm-3">Caracteristicas:</dt>
  <dd class="col-sm-9">Caracteristicas del equipo</dd>

 </dl>

 

</div>
</section>
@endsection