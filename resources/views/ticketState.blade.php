@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Estado de solicitudes</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Estado de solicitudes</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    
     <section class="section dashboard">
      <div class="row">


  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Solicitudes realizadas
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Solicitudes</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Ticket</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Asunto</th>
                        <th scope="col">Lugar</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Estado</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                         <tr>
                        <th scope="row"><a>#2457</a></th>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td>Oficina monitoreo</td>
                        <td>Descripcion de lo que sucede con el problema que se esta denunciando por parte del propietario</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                      

                    </tbody>
                  </table>

                </div>

             
            </div><!-- End Recent Sales -->
        </div>
    </section>
@endsection