@extends('layouts.plantilla')
@section('contenido')

    @if( session('mensaje') )
        <div class="alert alert-success">
            {{ session('mensaje') }}
        </div>
    @endif

    <div class="pagetitle">
        <h1>Formulario diario</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">MKS</a></li>
                <li class="breadcrumb-item active">Formulario diario</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
                Informacion
            </div>

            <form class="row g-3 needs-validation" action="/contratista/store" method="post" enctype="multipart/form-data" novalidate>
                @csrf

                <input  for="ticPropietario" id="ticPropietario" name="ticPropietario" type="hidden" value="2">



                <div class="col-md-4 position-relative">
                    <label for="conUbicacion" class="form-label">Ubicacion del servidor </label>
                    <select class="form-select" name="conUbicacion" id="conUbicacion" required>
                        <option selected disabled value="">Seleccione un lugar</option>
                        @foreach( $ubicacions as $ubicacion )
                            <option @selected( old('idUbicacion')==$ubicacion->idUbicacion ) value="{{ $ubicacion->idUbicacion }}">{{ $ubicacion->ubiNombre }}</option>
                        @endforeach

                    </select>
                    <div class="invalid-tooltip">
                        Por favor seleccione un lugar
                    </div>
                </div>

                <div class="col-md-8 position-relative">
                </div>



                <div class="col-md-12 position-relative">
                    <label for="conObservaciones" class="form-label">Observaciones</label>
                    <textarea class="form-control" id="conObservaciones" name="conObservaciones" placeholder="Indique observaciones" required>{{old('conObservaciones')}}</textarea>
                    <div class="invalid-feedback">
                        Por favor complete el campo de observaciones
                    </div>
                </div>


                <div class="col-12">
                    <button class="btn btn-primary">Enviar</button>
                </div>
            </form>

        </div>
        @include( 'layouts.msgValidacion' )
    </section>

@endsection
