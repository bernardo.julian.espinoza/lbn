@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Nuevo patrimonio</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Patrimonio</a></li>
          <li class="breadcrumb-item active">Nuevo patrimonio</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Datos del patrimonio
</div>
  
<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-2 position-relative">
    <label for="validationTooltip03" class="form-label">CODIGO</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique codigo" required>
    <div class="invalid-tooltip">
      Numero de inventario infraestructura
    </div>
  </div>

  <div class="col-md-5 position-relative">
    <label for="validationTooltip03" class="form-label">Serie</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique serie" required>
    <div class="invalid-tooltip">
      Numero de serie
    </div>
  </div>

  <div class="col-md-5 position-relative">
    <label for="validationTooltip03" class="form-label">Patrimonio</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique patrimonio">
    <div class="invalid-tooltip">
      Numero de patrimonio
    </div>
  </div>



  <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Marca</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Seleccione una marca
    </div>
  </div>

 <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Modelo</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Seleccione un modelo
    </div>
  </div>
   <div class="col-md-4 position-relative">
    <label for="validationTooltip04" class="form-label">Tipo</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Seleccione un tipo
    </div>
  </div> 


   <div class="col-md-12 position-relative">
    <label for="validationTextarea" class="form-label">Caracteristicas</label>
    <textarea class="form-control" id="validationTextarea" placeholder="Indique las caracteristicas del patrimonio" required></textarea>
    <div class="invalid-feedback">
      Por favor complete el campo de caracteristicas
    </div>
  </div>


  <div class="col-12">
    <button class="btn btn-primary" type="submit">Registrar</button>
  </div>
</form>



</div>
</section>


@endsection