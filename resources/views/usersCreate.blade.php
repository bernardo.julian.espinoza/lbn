@extends('layouts.plantilla')
@section('contenido')


    

<div class="pagetitle">
      <h1>Nuevo ticket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Nuevo ticket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

<section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Informacion
</div>
  
<form class="row g-3 needs-validation" novalidate>

	<div class="col-md-9 position-relative">
    <label for="validationTooltip03" class="form-label">Resumen</label>
    <input type="text" class="form-control" id="validationTooltip03" placeholder="Indique un resumen" required>
    <div class="invalid-tooltip">
      Por favor complete el resumen
    </div>
  </div>



  <div class="col-md-3 position-relative">
    <label for="validationTooltip04" class="form-label">Lugar</label>
    <select class="form-select" id="validationTooltip04" required>
      <option selected disabled value="">Choose...</option>
      <option>...</option>
    </select>
    <div class="invalid-tooltip">
      Por favor seleccione un lugar
    </div>
  </div>

  <div class="col-md-6 position-relative">
  </div>

   <div class="col-md-12 position-relative">
    <label for="validationTextarea" class="form-label">Descripcion</label>
    <textarea class="form-control" id="validationTextarea" placeholder="Indique una descripcion" required></textarea>
    <div class="invalid-feedback">
      Por favor complete el campo descripcion
    </div>
  </div>


  <div class="col-12">
    <button class="btn btn-primary" type="submit">Enviar</button>
  </div>
</form>

</div>
</section>

@endsection
