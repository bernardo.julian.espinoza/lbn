<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>INFRANET</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="/img/favicon.png" rel="icon">
  <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

 
  <!-- Vendor CSS Files -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="/css/style.css" rel="stylesheet">


</head>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a href="index.html" class="logo d-flex align-items-center w-auto">
                  <img src="/img/fv1.png" alt="">
                  <span class="d-none d-lg-block">Ferrovías</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Accede a tu cuenta</h5>
                    <p class="text-center small">Ingresa tu usuario y contraseña!</p>
                  </div>

                  <form class="row g-3 needs-validation" action="{{ route('login') }}" method="post">
                    @csrf

                    <div class="col-12">
                      <label for="email" class="form-label" :value="__('Email')">Email corporativo</label>
                      <div class="input-group has-validation">

                        <input type="email" name="email" class="form-control" id="email" :value="old('email')" required autofocus >
                        <div class="invalid-feedback">Por favor, ingresa tu correo.</div>
                      </div>
                    </div>

                    <div class="col-12">
                      <label for="password" class="form-label">Contraseña</label>
                      <input type="password" name="password" class="form-control" id="password" :value="__('Password')" required autocomplete="current-password">
                      <div class="invalid-feedback">Por favor, ingresa tu contraseña.</div>
                    </div>

                    <div class="col-12">
                    <div id="alerta"></div>
                    </div>
                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit">{{ __('Log in') }}</button>
                    </div>
                                       
                  </form>
                 

                </div>
              </div>
             
              <div class="credits">

                    <p class="small mb-0">Perdiste la clave? 

                       @if (Route::has('password.request'))
                       <a href="{{ route('password.request') }}">Recupera tu contraseña!</a>
                       @endif

                   </p>

                    <br>
              </div>

              <div class="credits">
                Backend developed by <a href="https://bje.com/">Bernardo Espinoza</a>
              </div>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


  <script src="/js/validate.js" type="text/javascript"></script> 

</body>

</html>



        <!-- Session Status 
        <x-auth-session-status class="mb-4" :status="session('status')" />

        Validation Errors 
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

       -->