@extends('layouts.plantilla')
@section('contenido')



<div class="pagetitle">
      <h1>Bandeja de tickets</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Bandeja de tickets</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  REDES
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Asignados al sector</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Ticket</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Asunto</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">Lugar</th>
                        <th scope="col">Estado</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a href="/ticket/read" class="text-primary">#2457</a></th>
                        <td><a>Javier Perez</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin visual de camaras</td>
                        <td><a>Video</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>        
                      </tr>
                      <tr>
                         <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Tren Control</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Video</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td><a>Señalamiento</a></td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>
                      <tr>
                        <th scope="row"><a href="#" class="text-primary">#2457</a></th>
                        <td><a>Bernardo Espinoza</a></td>
                        <td>2/3/2022</td>
                        <td>Pc sin video</td>
                        <td>Video</td>
                        <td>Oficina monitoreo</td>
                        <td><span class="badge bg-success">Abierto</span></td>
                      </tr>

                    </tbody>
                  </table>

                </div>

             
            </div><!-- End Recent Sales -->

          </div>
        </section>
@endsection