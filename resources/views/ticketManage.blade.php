@extends('layouts.plantilla')
@section('contenido')


<div class="pagetitle">
      <h1>Gestion de tickets</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item active">Gestion de tickets</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Tickets abiertos
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Sin asignar</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Ticket</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Creado el</th>
                        <th scope="col">Asunto</th>
                        <th scope="col">Zona de incidencia</th>
                        <th scope="col">Estado</th>                        
                      </tr>
                    </thead>

                    
                    <tbody>
                     @foreach( $tickets as $ticket )
                      <tr>
                        <th scope="row"><a href="/ticket/read/{{$ticket->idTicket}}" class="text-primary">{{$ticket->idTicket}}</a></th>
                        <td><a>{{$ticket->getPropietario->proNombre }}</a></td>
                        <td>{{$ticket->created_at}}</td>
                        <td>{{$ticket->ticResumen}}</td>
                        <td>{{$ticket->getUbicacion->ubiNombre}}</td>
                        <td><span class="{{$ticket->getEstado->estClass}}">{{$ticket->getEstado->estNombre}}</span></td>        
                      </tr>
                    @endforeach
                   
                    </tbody>

                  </table>

                </div>

             
            </div><!-- End Recent Sales -->
            <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Tickets en proceso
</div>

<!-- Recent Sales -->
            <div class="col-12">
                    

                <div class="card-body">
                  <h5 class="card-title">Tickets <span>| Asignados</span></h5>

                  <table class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">Ticket</th>
                        <th scope="col">Propietario</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Asunto</th>
                        <th scope="col">Asignado a</th>
                        <th scope="col">Estado</th>                        
                      </tr>
                    </thead>
                    <tbody>
                     @foreach( $tics as $tic )
                      <tr>
                        <th scope="row"><a href="/ticket/process/{{$tic->idTicket}}" class="text-primary">{{$tic->idTicket}}</a></th>
                        <td>{{$tic->getPropietario->proNombre}}</td>
                        <td>{{$tic->created_at}}</td>
                        <td>{{$tic->ticResumen}}</td>
                        <td>{{$tic->getCategoria->catNombre}}</td>
                        <td><span class="{{$tic->getEstado->estClass}}">{{$tic->getEstado->estNombre}}</span></td>        
                      </tr>
                    @endforeach
                   
                    </tbody>
                  </table>

                </div>

             
            </div><!-- End Recent Sales -->
          </div>
        </section>
@endsection