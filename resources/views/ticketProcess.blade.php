@extends('layouts.plantilla')
@section('contenido')

<div class="pagetitle">
      <h1>Estado del ticket n° {{$ticket->idTicket}}</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Tickets</a></li>
          <li class="breadcrumb-item"><a href="#">Gestion de tickets</a></li>
          <li class="breadcrumb-item active">Datos del ticket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section dashboard">
      <div class="row">

  <div class="h5 pb-2 mb-4 text-primary border-bottom border-primary ">
  Detalles del ticket
</div>

<dl class="row">

  <dt class="col-sm-4">Propietario:</dt>
  <dd class="col-sm-8">{{$ticket->getPropietario->proNombre}}</dd>

  <dt class="col-sm-4">Asunto:</dt>
  <dd class="col-sm-8">{{$ticket->ticResumen}}</dd>

  <dt class="col-sm-4">Lugar de intervencion:</dt>
  <dd class="col-sm-8">{{$ticket->getUbicacion->ubiNombre}}</dd>

  <dt class="col-sm-4">Descripcion del problema:</dt>
  <dd class="col-sm-8">{{$ticket->ticDescripcion}}</dd>

  <dt class="col-sm-4">Prioridad asignada:</dt>
  <dd class="col-sm-8">{{$ticket->getPrioridad->priNombre}}</dd>

  <dt class="col-sm-4">Sector asignado:</dt>
  <dd class="col-sm-8">{{$ticket->getCategoria->catNombre}}</dd>

  <dt class="col-sm-4">Supervisor a cargo:</dt>
  <dd class="col-sm-8">{{$ticket->getPropietario->proNombre}}</dd>

  <dt class="col-sm-4">Detalles agregados:</dt>
  <dd class="col-sm-8">{{$ticket->ticDetalle}}</dd>

  <dt class="col-sm-4">Tecnico a cargo:</dt>
  <dd class="col-sm-8">{{$ticket->getPropietario->proNombre}}</dd>

  <dt class="col-sm-4">Observaciones del tecnico:</dt>
  <dd class="col-sm-8">{{$ticket->ticObservaciones}}</dd>

  <dt class="col-sm-4">Solicitud creada el:</dt>
  <dd class="col-sm-8">{{$ticket->created_at}}</dd>

  <dt class="col-sm-4">Ultima actualizacion el:</dt>
  <dd class="col-sm-8">{{$ticket->updated_at}}</dd>

  <dt class="col-sm-4">Estado actual de la solicitud:</dt>
  <dd class="col-sm-8"><span class="{{$ticket->getEstado->estClass}}">{{$ticket->getEstado->estNombre}}</span></dd>


 </dl>




</div>
</section>
@endsection