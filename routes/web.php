<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


use App\Http\Controllers\User;


Route::get('/user/create',[User::class,'create']);

Route::get('/user/read',[User::class,'read']);

Route::get('/user/delete',[User::class,'delete']);

Route::get('/user/update',[User::class,'update']);

Route::get('/user/manage',[user::class,'manage']);

Route::get('/user/managegroup',[user::class,'managegroup']);

Route::get('/user/creategroup',[user::class,'creategroup']);


use App\Http\Controllers\TicketController;

//VIEWS

Route::get('/ticket/create',[TicketController::class,'create']);

Route::post('/ticket/store',[TicketController::class,'store']);

Route::put('/ticket/update',[TicketController::class,'update']);

Route::get('/ticket/manage',[TicketController::class,'manage']);

Route::get('/ticket/search',[TicketController::class,'search']);

Route::get('/ticket/state',[TicketController::class,'state']);

Route::get('/ticket/report',[TicketController::class,'report']);

Route::get('/ticket/statistics',[TicketController::class,'statistics']);

Route::get('/ticket/inbox',[TicketController::class,'inbox']);

Route::get('/ticket/search',[TicketController::class,'search']);

Route::get('/ticket/read/{id}',[TicketController::class,'read']);

Route::get('/ticket/process/{id}',[TicketController::class,'process']);

Route::get('/ticket/list',[TicketController::class,'list']);



use App\Http\Controllers\PatrimonioController;

//VIEWS

Route::get('/patrimonio/create',[PatrimonioController::class,'create']);

Route::get('/patrimonio/search',[PatrimonioController::class,'search']);

Route::get('/patrimonio/brand',[PatrimonioController::class,'brand']);

Route::get('/patrimonio/model',[PatrimonioController::class,'model']);

Route::get('/patrimonio/report',[PatrimonioController::class,'report']);

Route::get('/patrimonio/statistics',[PatrimonioController::class,'statistics']);

Route::get('/patrimonio/read',[PatrimonioController::class,'read']);


use App\Http\Controllers\TrazabilidadController;

//VIEWS

Route::get('/trazabilidad/move',[TrazabilidadController::class,'move']);

Route::get('/trazabilidad/manage',[TrazabilidadController::class,'manage']);

Route::get('/trazabilidad/search',[TrazabilidadController::class,'search']);

Route::get('/trazabilidad/report',[TrazabilidadController::class,'report']);

Route::get('/trazabilidad/statistics',[TrazabilidadController::class,'statistics']);

Route::get('/trazabilidad/read',[TrazabilidadController::class,'read']);

Route::get('/trazabilidad/operation',[TrazabilidadController::class,'operation']);



use App\Http\Controllers\ContratistaController;

//VIEWS

Route::get('/contratista/dailyform',[ContratistaController::class,'dailyform']);

Route::get('/contratista/store',[ContratistaController::class,'store']);

Route::get('/contratista/semesterform',[ContratistaController::class,'semesterform']);

Route::get('/contratista/annualreport',[ContratistaController::class,'annualreport']);



