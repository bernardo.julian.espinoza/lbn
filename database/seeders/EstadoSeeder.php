<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::insert(
            [
                [ 'estNombre'=>'Abierto','estClass'=>'badge bg-primary','estAbreviado'=>'ABIE' ],
                [ 'estNombre'=>'Transferido','estClass'=>'badge bg-success','estAbreviado'=>'TRAN' ],
                [ 'estNombre'=>'Asignado','estClass'=>'badge bg-warning','estAbreviado'=>'ASIG' ],
                [ 'estNombre'=>'Pendiente','estClass'=>'badge bg-danger','estAbreviado'=>'PEND' ],
                [ 'estNombre'=>'Cerrado','estClass'=>'badge bg-dark','estAbreviado'=>'CERR' ]

            ]
        );
    }
}
