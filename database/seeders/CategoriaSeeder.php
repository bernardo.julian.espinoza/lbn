<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Categoria::insert(
            [   
                [ 'catNombre'=>'Indefinida','catAbreviado'=>'INDF','catSector'=>'0' ],
                [ 'catNombre'=>'Redes','catAbreviado'=>'REDS','catSector'=>'2' ],
                [ 'catNombre'=>'Video','catAbreviado'=>'VIDE','catSector'=>'2' ],
                [ 'catNombre'=>'Voip','catAbreviado'=>'VOIP','catSector'=>'3' ],
                [ 'catNombre'=>'Tren control','catAbreviado'=>'TREN','catSector'=>'4' ],
                [ 'catNombre'=>'Base de datos','catAbreviado'=>'BASE','catSector'=>'4' ],
                [ 'catNombre'=>'Desarrollo','catAbreviado'=>'DEVS','catSector'=>'2' ],
                [ 'catNombre'=>'Energia','catAbreviado'=>'ENER','catSector'=>'3' ],
                [ 'catNombre'=>'Control de acceso','catAbreviado'=>'CONT','catSector'=>'4' ],
                [ 'catNombre'=>'Señalamiento','catAbreviado'=>'SEÑA','catSector'=>'5' ],
                [ 'catNombre'=>'Internos','catAbreviado'=>'INTE','catSector'=>'3' ],
                [ 'catNombre'=>'Devops','catAbreviado'=>'DEVO','catSector'=>'4' ],
                [ 'catNombre'=>'Camaras IP','catAbreviado'=>'CAMI','catSector'=>'5' ]

            ]
        );
    }
}
