<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Propietario;

class PropietarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Propietario::insert(
            [
                [ 'proNombre'=>'ND','proApellido'=>'ND','proDNI'=>'00000000' ],
                [ 'proNombre'=>'Bernardo','proApellido'=>'Espinoza','proDNI'=>'31098711' ],
                [ 'proNombre'=>'Julian','proApellido'=>'Julian','proDNI'=>'38244117' ],
                [ 'proNombre'=>'Espinoza','proApellido'=>'Bernardo','proDNI'=>'10961922' ]

            ]
        );
    }
}
