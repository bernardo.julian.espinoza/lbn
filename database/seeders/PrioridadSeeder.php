<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Prioridad;


class PrioridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
          Prioridad::insert(
            [
                [ 'priNombre'=>'Indefinida','priAbreviado'=>'INDF' ],
                [ 'priNombre'=>'Muy alta','priAbreviado'=>'MALT' ],
                [ 'priNombre'=>'Alta','priAbreviado'=>'ALTA' ],
                [ 'priNombre'=>'Media','priAbreviado'=>'MEDI' ],
                [ 'priNombre'=>'Baja','priAbreviado'=>'BAJA' ],
                [ 'priNombre'=>'Muy baja','priAbreviado'=>'MBAJ' ]

            ]
        );
    
    }
}
