<?php

namespace Database\Seeders;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ubicacion;

class UbicacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Ubicacion::insert(
            [
                [ 'ubiNombre'=>'Estacion Retiro','ubiAbreviado'=>'ERET' ],
                [ 'ubiNombre'=>'Estacion Saldias','ubiAbreviado'=>'ESAL' ],
                [ 'ubiNombre'=>'Estacion Ciudad universitaria','ubiAbreviado'=>'ECIU' ],
                [ 'ubiNombre'=>'Estacion Aristobulo Del Valle','ubiAbreviado'=>'EADV' ],
                [ 'ubiNombre'=>'Estacion Padilla','ubiAbreviado'=>'EPAD' ],
                [ 'ubiNombre'=>'Estacion Florida','ubiAbreviado'=>'EFLO' ],
                [ 'ubiNombre'=>'Estacion Munro','ubiAbreviado'=>'MUN' ],
                [ 'ubiNombre'=>'Estacion Carapachay','ubiAbreviado'=>'ECAR' ],
                [ 'ubiNombre'=>'Estacion Villa Adelina','ubiAbreviado'=>'EVAD' ],
                [ 'ubiNombre'=>'Estacion Boulogne','ubiAbreviado'=>'EBOU' ],
                [ 'ubiNombre'=>'Estacion Vicealmirante Montes','ubiAbreviado'=>'EMON' ],
                [ 'ubiNombre'=>'Estacion Don Torcuato','ubiAbreviado'=>'EDTC' ],
                [ 'ubiNombre'=>'Estacion Adolfo Sourdeaux','ubiAbreviado'=>'ESOU' ],
                [ 'ubiNombre'=>'Estacion Villa De Mayo','ubiAbreviado'=>'EVDM' ],
                [ 'ubiNombre'=>'Estacion Los Polvorines','ubiAbreviado'=>'EPOL' ],
                [ 'ubiNombre'=>'Estacion Pablo Nogues','ubiAbreviado'=>'ENOG' ],
                [ 'ubiNombre'=>'Estacion Grand Bourg','ubiAbreviado'=>'EGBR' ],
                [ 'ubiNombre'=>'Estacion Tierras Altas','ubiAbreviado'=>'ETAL' ],
                [ 'ubiNombre'=>'Estacion Tortuguitas','ubiAbreviado'=>'ETRT' ],
                [ 'ubiNombre'=>'Estacion Manuel Alberti','ubiAbreviado'=>'EALB' ],
                [ 'ubiNombre'=>'Estacion Del Viso','ubiAbreviado'=>'EDVS' ],
                [ 'ubiNombre'=>'Estacion Villa Rosa','ubiAbreviado'=>'EVRO' ],
                [ 'ubiNombre'=>'Oficina Infraestructura','ubiAbreviado'=>'OINF' ],
                [ 'ubiNombre'=>'Oficina Transporte','ubiAbreviado'=>'OTRA' ],
                [ 'ubiNombre'=>'Oficina Gerencia','ubiAbreviado'=>'OGER' ],
                [ 'ubiNombre'=>'Oficina Evasion','ubiAbreviado'=>'OEVA' ],
                [ 'ubiNombre'=>'Oficina Segop','ubiAbreviado'=>'OSEG' ],
                [ 'ubiNombre'=>'Oficina Resguardo','ubiAbreviado'=>'ORES' ],
                [ 'ubiNombre'=>'Oficina Patrimonio','ubiAbreviado'=>'OPAT' ],
                [ 'ubiNombre'=>'Central Monitoreo','ubiAbreviado'=>'CMON' ],
                [ 'ubiNombre'=>'Central Telefonica Boulogne','ubiAbreviado'=>'CTEB' ],
                [ 'ubiNombre'=>'Central Teleinformatica','ubiAbreviado'=>'CTIN' ],
                [ 'ubiNombre'=>'Central Telefonica Retiro','ubiAbreviado'=>'CTER' ],
                [ 'ubiNombre'=>'Oficina Limpieza','ubiAbreviado'=>'OLIM' ],
                [ 'ubiNombre'=>'Oficina Audio','ubiAbreviado'=>'OAUD' ],
                [ 'ubiNombre'=>'Oficina Material Rodante','ubiAbreviado'=>'OMRO' ],
                [ 'ubiNombre'=>'Oficina Compras','ubiAbreviado'=>'OCOM' ],
                [ 'ubiNombre'=>'Almacen Infraestructura','ubiAbreviado'=>'AINF' ],
                [ 'ubiNombre'=>'Almacen Material Rodante','ubiAbreviado'=>'AMRO' ],
                [ 'ubiNombre'=>'Oficina Entrepiso','ubiAbreviado'=>'OENP' ],
                [ 'ubiNombre'=>'Taller Material Rodante','ubiAbreviado'=>'TMRO' ],
                [ 'ubiNombre'=>'Taller Video', 'ubiAbreviado'=>'TVID' ],
                [ 'ubiNombre'=>'Taller Pintura','ubiAbreviado'=>'TPIN' ],
                [ 'ubiNombre'=>'Taller Alistamiento','ubiAbreviado'=>'TALI' ],
                [ 'ubiNombre'=>'Oficina Recursos Humanos','ubiAbreviado'=>'ORRH' ],
                [ 'ubiNombre'=>'Central PCZ','ubiAbreviado'=>'CPCZ' ],
                [ 'ubiNombre'=>'Oficina Redes','ubiAbreviado'=>'ORED' ]

            ]
        );
    }
}
