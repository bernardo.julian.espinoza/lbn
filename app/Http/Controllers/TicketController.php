<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Prioridad;
use App\Models\Categoria;
use App\Models\Propietario;
use App\Models\Ubicacion;
use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inbox()
    {
        return view('ticketInbox');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function statistics()
    {
        return view('ticketStatistics');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        return view('ticketReport');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        $tickets=Ticket::with(['getPropietario','getUbicacion','getEstado'])->where('ticEstado','=',1)->get();
        $tics=Ticket::with(['getPropietario','getCategoria','getEstado'])->where('ticEstado','=',2)->get();

        return view('ticketManage',
            [
                'tickets'=>$tickets,
                'tics'=>$tics
            ]
        );
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function state()
    {
        return view('ticketState');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return view ('ticketSearch');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $ubicacions = Ubicacion::all();
      
        return view('ticketCreate',
                [
                    'ubicacions'=>$ubicacions
                ]
        );
        
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validarTicketForm($request);
        
        $Ticket = new Ticket;
        $Ticket->ticPropietario = $request->ticPropietario;
        $Ticket->ticResumen = $request->ticResumen;
        $Ticket->ticUbicacion = $request->ticUbicacion;
        $Ticket->ticDescripcion = $request->ticDescripcion;
        $Ticket->ticPrioridad = $ticPrioridad = 1;
        $Ticket->ticEstado = $ticEstado = 1;
        $Ticket->ticCategoria = $ticCategoria = 1;
        $Ticket->ticSupervisor = $ticSupervisor = 1;
        $Ticket->ticDetalle = $ticDetalle = 'No definido';
        $Ticket->ticTecnico = $ticTecnico = 1;
        $Ticket->ticObservaciones = $ticObservaciones = 'No definido';
        $Ticket->save();

                
        return redirect('/ticket/create')
            ->with(['mensaje'=>'Usted ha creado un nuevo ticket de asistencia - El mismo ya se encuentra en proceso para su asistencia ...']);
    }


private function validarTicketForm( Request $request )
    {
        $request->validate(
            [
                'ticResumen' => 'required|min:2|max:70',
                'ticUbicacion' => 'required',
                'ticDescripcion' => 'required|min:3|max:300'
            ],
            [
                'ticResumen.required' => 'El campo RESUMEN es requerido.',
                'ticResumen.min'=>'El campo RESUMEN debe tener como mínimo 2 caractéres.',
                'ticResumen.max'=>'El campo RESUMEN debe tener 70 caractéres como máximo.',
                'ticUbicacion.required'=>'El campo UBICACION es requerido.',
                'prdDescripcion.required'=>'El campo DESCRIPCION es requerido.',
                'prdDescripcion.min'=>'El campo DESCRIPCION debe tener como mínimo 2 caractéres.',
                'prdDescripcion.max'=>'El campo DESCRIPCION debe tener 300 caractéres como máximo.'
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function read( $id )
    {
        $ticket=Ticket::find( $id );
        $propietarios=Propietario::all();
        $ubicacions=Ubicacion::all();
        $prioridads=Prioridad::all();
        $estados=Estado::all();
        $categorias=Categoria::all();


        return view('ticketRead',[

            'ticket'=>$ticket,
            'propietarios'=>$propietarios,
            'ubicacions'=>$ubicacions,
            'prioridads'=>$prioridads,
            'estados'=>$estados,
            'categorias'=>$categorias


        ]
    );
    }

    public function process($id)
    {
        $ticket=Ticket::find( $id );
        $propietarios=Propietario::all();
        $ubicacions=Ubicacion::all();
        $prioridads=Prioridad::all();
        $estados=Estado::all();
        $categorias=Categoria::all();


        return view('ticketProcess',[

            'ticket'=>$ticket,
            'propietarios'=>$propietarios,
            'ubicacions'=>$ubicacions,
            'prioridads'=>$prioridads,
            'estados'=>$estados,
            'categorias'=>$categorias


        ]
    );
    }


 

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $idTicket=$request->idTicket;
        $Ticket=Ticket::find( $request->idTicket );
        $Ticket->ticCategoria=$request->ticCategoria;
        $Ticket->ticPrioridad=$request->ticPrioridad;
        $Ticket->ticEstado=$ticEstado = 2;
        $Ticket->ticSupervisor=$request->ticSupervisor;
        $Ticket->ticDetalle=$request->ticDetalle;
        $Ticket->save();
        return redirect('/ticket/manage')
            ->with(['mensaje'=>'El ticket: '.$idTicket.'fue transferido con exito...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }


}
