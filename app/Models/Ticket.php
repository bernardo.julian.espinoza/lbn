<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $primaryKey = 'idTicket';
    
    public function getPropietario()

    {
        return $this->belongsTo(
            Propietario::class,
            'ticPropietario',
            'idPropietario'
        );

    }

    public function getUbicacion()

    {
        return $this->belongsTo(
            Ubicacion::class,
            'ticUbicacion',
            'idUbicacion'
        );
    }

    public function getPrioridad()

    {
        return $this->belongsTo(
            Prioridad::class,
            'ticPrioridad',
            'idPrioridad'
        );
    }

    public function getEstado()

    {
        return $this->belongsTo(
            Estado::class,
            'ticEstado',
            'idEstado'
        );
    }

    public function getCategoria()

    {
        return $this->belongsTo(
            Categoria::class,
            'ticCategoria',
            'idCategoria'
        );
    }


}
